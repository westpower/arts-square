jQuery(document).ready(function($) {
  $('#design-slider').addClass('owl-carousel owl-wp');
  $('#design-slider').owlCarousel({
    center: true,
    loop: true,
    nav: true,
    dots: true,
    smartSpeed: 600,
    responsive: {
      0: {
        items: 1,
        stagePadding: 60
      },
      576: {
        items: 1,
        stagePadding: 120
      },
      768: {
        items: 3,
        stagePadding: 0
      }
    }
  });

  const navWidth = $('#design-slider .owl-dots').width();
  $('#design-slider .owl-nav').css('width', navWidth + 80 + 'px');
});


jQuery(document).ready(function($) {
  $('#discount-slider').addClass('owl-carousel owl-wp');
  $('#discount-slider').owlCarousel({
    center: true,
    items: 1,
    loop: true,
    nav: true,
    dots: true,
    smartSpeed: 600
  });

  const navWidth = $('#discount-slider .owl-dots').width();
  $('#discount-slider .owl-nav').css('width', navWidth + 80 + 'px');
});


jQuery(document).ready(function($) {
  $('#details-slider').addClass('owl-carousel owl-wp');
  $('#details-slider').owlCarousel({
    center: true,
    items: 1,
    loop: true,
    nav: true,
    dots: true,
    smartSpeed: 600
  });

  const navWidth = $('#details-slider .owl-dots').width();
  $('#details-slider .owl-nav').css('width', navWidth + 80 + 'px');

  const contentLeft = $('#details-slider .slider-content')[0].offsetLeft;
  $('#details-slider .owl-nav').css('left', contentLeft + 'px');
  $('#details-slider .owl-dots').css('left', contentLeft + 40 + 'px');
});

jQuery(document).ready(function($) {
  $('#banks-slider').addClass('owl-carousel owl-wp');
  $('#banks-slider').owlCarousel({
    center: true,
    loop: true,
    nav: true,
    dots: true,
    smartSpeed: 600,
    responsive: {
      0: {
        items: 1
      },
      578: {
        items: 3
      },
      768: {
        items: 5
      }
    }
  });

  const navWidth = $('#banks-slider .owl-dots').width();
  $('#banks-slider .owl-nav').css('width', navWidth + 80 + 'px');
});


jQuery(document).ready(function($) {
  var floorSlider = $('#floor-slider');
  floorSlider.addClass('owl-carousel owl-wp');
  floorSlider.owlCarousel({
    items: 1,
    center: true,
    loop: false,
    nav: true,
    dots: false,
    smartSpeed: 600,
    animateOut: 'fadeOutDown',
    animateIn: 'fadeInDown'
  }, initFloorSlider());

  floorSlider.on('changed.owl.carousel', function(event) {
    $('.current-floor').text(event.item.index + 1);
    $('.total-floors .floors li').removeClass('active');
    $('.total-floors .floors li')[event.item.index].classList.add('active');
  });

  function initFloorSlider() {
    setTimeout(function() {
      var curNav = $('#floor-slider .owl-nav .owl-prev');
      var curFloor = document.createElement('div');
      $(curFloor).addClass('current-floor');
      $(curFloor).text('1');
      $(curNav).after(curFloor);
    });
  }
});

// jQuery(document).ready(function($) {

//   function initFiltersCarousel() {

//     const filterLists = [...document.querySelectorAll('.filterby-list')];

//     filterLists.forEach((el, i, arr) => {
//       console.log(el);
//       const listWidth = el.offsetWidth;
  
//       let listItemsWidth = [...el.querySelectorAll('.filterby-list__item')]
//         .reduce((sum, current) => {
//           return sum + current.offsetWidth + 32;
          
//         }, 0);
  
//         listItemsWidth -= 32;
//         console.log(listItemsWidth, listWidth);
    
//         //if (listItemsWidth <= listWidth) return;

//         console.log(arr);
        
//         //.owlCarousel();
//     });
//   };

//   //initFiltersCarousel();
// });


console.log('main');

jQuery(document).ready(function($) {
  $("#mortgage-cost").ionRangeSlider({
    type: 'single',
    min: 300000,
    max: 20000000,
    from: 3200000,
    grid: false
  });

  $("#mortgage-payment").ionRangeSlider({
    type: 'single',
    min: 300000,
    max: 20000000,
    from: 1000000,
    grid: false
  });
});

jQuery(document).ready(function($) {
  $('.burger-btn').on('click', function() {
    
    if($(this).hasClass('open')) {
      $('.overlay').removeClass('show');
      $(this).removeClass('open');
      $('main').removeClass('blur');
      $('body').removeClass('menu-shown');
      $('.full-menu-block').removeClass('show');
      return;
    }

    $('.overlay').addClass('show');
    $(this).addClass('open');
    $('main').addClass('blur');
    $('body').addClass('menu-shown');
    $('.full-menu-block').addClass('show');
    
  });
});

jQuery(document).ready(function($) {
  $('select').niceSelect();

});