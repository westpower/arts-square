# ЖК Площадь искусств #

- [Ссылка на макет](https://xd.adobe.com/view/d9c5c284-9ce4-419d-738d-72735f7dfe1a-aff4/)
- [Демка первого экрана](https://demo.skyweb24.ru/interactivepictures/example1.php)


### Как это готовить ###

- `git clone git@bitbucket.org:westpower/arts-square.git`
- `cd arts-square`
- `npm install`
- `gulp`


![alt text](https://brodude.ru/wp-content/uploads/2014/03/brodude.ru_14.03.2014_edpyb0Ni5CCdm.jpg)