// jshint ignore: start
'use strict';
var gulp   = require('gulp'),
		chalk  = require('chalk'),
		plugin = require('gulp-load-plugins')(),
		pie = require('postcss-pie');
plugin.browserSync = require('browser-sync');
plugin.rimraf = require('rimraf');
plugin.vinylFtp = require('vinyl-ftp');
plugin.imagemin.mozjpeg = require('imagemin-mozjpeg');
plugin.imagemin.pngquant = require('imagemin-pngquant');

// Chalk colors: black red green yellow blue magenta cyan white gray redBright greenBright yellowBright blueBright magentaBright cyanBright whiteBright

var path = {
	build: {
		html:  'build/',
		php:   'build/',
		js:    'build/js/',
		css:   'build/',
		img:   'build/images/',
		fonts: 'build/fonts/',
		maps:  'build/maps/',
		svg:   'build/svg/'
	},
	src:   {
		html:  'src/*.html',
		php:   'src/**/*.php',
		js:    'src/js/**/*.js',
		css:   'src/**/*.scss',
		img:   'src/images/**/*',
		fonts: 'src/fonts/**/*.*',
		svg:   'src/svg/**/*'
	},
	watch: {
		html:  'src/**/*.html',
		php:   'src/**/*.php',
		js:    'src/js/**/*.js',
		css:   'src/**/*.scss',
		img:   'src/images/**/*',
		fonts: 'src/fonts/**/*.*',
		svg:   'src/svg/**/*'
	},
	clean:  './build',
	update: './package.json',
	ftp: '/'
};

var browserSyncConfig = {
	server:    {
		baseDir: './build'
	},
	tunnel:    false,
	host:      'localhost',
	// proxy:     'localhost/project-placeholder/build/',
	port:      9000,
	logPrefix: 'Frontend'
};

// var ftpConnect = plugin.vinylFtp.create({
// 	host: 			'poketone.info',
//     user: 			'masalbion_poketone',
//     password: 		'Qh1uIsVLolpZ',
// 	parallel: 		4,
// 	maxConnections: 8,
// 	port:			21,
// 	log:			'gutil.log'
// });

gulp.task('webserver', function () {
	plugin.browserSync(browserSyncConfig);
});

gulp.task('html', function () {
	gulp.src(path.src.html)
        // .pipe(plugin.changed(path.build.html))
		.pipe(plugin.plumber({
			errorHandler: plugin.notify.onError("Ошибка: <%= error.message %>")
		})).on('end', function(){
			plugin.util.log(chalk.red('Инициализирован обработчик ошибок HTML'));
		})
		.pipe(plugin.rigger()).on('end', function(){
			plugin.util.log(chalk.red('Подключены внешние файлы HTML'));
		})
    //     .pipe(plugin.htmlmin({ // Описание настроек тут: https://github.com/kangax/html-minifier
    //         collapseWhitespace: true,
    //         minifyJS: true,
    //         minifyCSS: true
    //     })).on('end', function(){
		// 	plugin.util.log(chalk.red('Минифицирован HTML'));
		// })
		.pipe(gulp.dest(path.build.html))
        .on('end', function(){
			plugin.util.log(chalk.bgRed('Файлы HTML скопированы в папку проекта'));
		})
        .pipe(plugin.browserSync.stream());
    //gulp.src(path.build.html + '/*.html')
		//.pipe(ftpConnect.newer(path.build.html, { base: '.', buffer: false }))
		//.pipe(ftpConnect.dest(path.ftp))
		// .on('end', function(){
		// 	plugin.util.log(chalk.blue('HTML файлы загружены на FTP'));
		// });
});

gulp.task('php', function() {
	gulp.src(path.src.php)
		.pipe(gulp.dest(path.build.php));
});

gulp.task('js', function () {
	gulp.src(path.src.js)
        .pipe(plugin.changed(path.build.js))
		.pipe(plugin.plumber({
			errorHandler: plugin.notify.onError("Ошибка: <%= error.message %>")
		})).on('end', function(){
			plugin.util.log(chalk.yellow('Инициализирован обработчик ошибок JavaScript'));
		})
		// .pipe(plugin.sourcemaps.init({
		// 	loadMaps: true
		// })).on('end', function(){
		// 	plugin.util.log(chalk.yellow('Инициализирован обработчик карт'));
		// })
		.pipe(gulp.dest(path.build.js))
    .pipe(plugin.browserSync.stream())
		.on('end', function(){
			plugin.util.log(chalk.black.bgYellow('Скрипты JavaScript скопированы в папку проекта'));
		})
		// .pipe(plugin.rename({
		// 	suffix: '.min'
		// }))
		// .pipe(plugin.uglify())
		// .pipe(plugin.sourcemaps.write('../maps/'))
		// .pipe(gulp.dest(path.build.js))
		// .on('end', function(){
		// 	plugin.util.log(chalk.yellow('Созданы минифицированные версии скриптов JavaScript'));
		// })
		//.pipe(plugin.browserSync.stream());
	// gulp.src(path.build.js + '/*.js')
	// 	.pipe(ftpConnect.newer(path.build.js, { base: '.', buffer: false }))
	// 	.pipe(ftpConnect.dest(path.ftp + '/js'))
	// 	.on('end', function(){
  //           plugin.util.log(chalk.blue('Скрипты JavaScript загружены на FTP'));
  //       });
    // gulp.src(path.build.maps + '/*.js.map')
	// 	.pipe(ftpConnect.newer(path.build.js, { base: '.', buffer: false }))
	// 	.pipe(ftpConnect.dest(path.ftp + '/maps'))
	// 	.on('end', function(){ plugin.util.log(chalk.blue('Карты JavaScript загружены на FTP')); });
});

gulp.task('css', function () {
	gulp.src(path.src.css)
        .pipe(plugin.changed(path.build.css))
		.pipe(plugin.plumber({
			errorHandler: plugin.notify.onError("Ошибка: <%= error.message %>")
		})).on('end', function(){
			plugin.util.log(chalk.green('Инициализирован обработчик ошибок CSS'));
		})
        .pipe(plugin.versionNumber({'replaces':[[/#{VERSION_REPlACE}#/g,'%TS%']]}))
        .on('end', function(){
			plugin.util.log(chalk.green('Поставлена временная метка в поле версии'));
		})
		// .pipe(plugin.sourcemaps.init({
		// 	loadMaps: true
		// })).on('end', function(){
		// 	plugin.util.log(chalk.green('Инициализирован обработчик карт'));
		// })
		.pipe(plugin.sass({
			// sourcemap: true
		})).on('end', function(){
			plugin.util.log(chalk.green('Обработан SASS'));
		})
        .pipe(plugin.postcss([
            require('postcss-gradientfixer')(),
            require('postcss-flexbugs-fixes')(),
            require('postcss-fixes')(),
            require('postcss-flexibility')(),
            require('postcss-color-rgba-fallback')(),
            require('postcss-cssnext')({
						browsers: ['last 10 versions'],
					}),
        ])).on('end', function(){
			plugin.util.log(chalk.green('Добавлены костыли для старых браузеров'));
		})
		.pipe(plugin.csscomb()).on('end', function(){
			plugin.util.log(chalk.green('Скомбинированы стили'));
		})
		.pipe(plugin.mergeMediaQueries({
			log: false
		})).on('end', function(){
			plugin.util.log(chalk.green('Скомпанованы @media'));
		})
        // .pipe(plugin.sourcemaps.write('./maps/')).on('end', function(){
		// 	plugin.util.log(chalk.green('Записаны карты для стилей'));
		// })
		.pipe(gulp.dest(path.build.css))
        .on('end', function(){
			plugin.util.log(chalk.bgGreen('Файлы CSS скопированы в папку проекта'));
		})
        .pipe(plugin.browserSync.stream())
		.pipe(plugin.rename({
			suffix: '.min'
		}))
		.pipe(plugin.cleanCss())
		// .pipe(plugin.sourcemaps.write('./maps/')).on('end', function(){
		// 	plugin.util.log(chalk.green('Записаны карты для минифицированных версий стилей'));
		// })
		.pipe(gulp.dest(path.build.css))
		.on('end', function(){
			plugin.util.log(chalk.green('Добавлена минифицированная версия CSS'));
		})
        .pipe(plugin.browserSync.stream());
	// gulp.src(path.build.css + '/*.css')
	// 	.pipe(ftpConnect.newer(path.build.css, { base: '.', buffer: false }))
	// 	.pipe(ftpConnect.dest(path.ftp))
	// 	.on('end', function(){
	// 		plugin.util.log(chalk.blue('Стили CSS загружены на FTP'));
	// 	});
});

gulp.task('img', function () {
	gulp.src(path.src.img)
        .pipe(plugin.newer(path.build.img))
		.pipe(plugin.plumber({
			errorHandler: plugin.notify.onError("Ошибка: <%= error.message %>")
		})).on('end', function(){
			plugin.util.log(chalk.cyan('Инициализирован обработчик ошибок изображений'));
		})
        .pipe(plugin.imagemin([
            plugin.imagemin.gifsicle({interlaced: true}),
            plugin.imagemin.jpegtran({progressive: true}),
            plugin.imagemin.mozjpeg({progressive: true}),
            plugin.imagemin.optipng({optimizationLevel: 7}),
            plugin.imagemin.pngquant({quality: '82-100'}),
            plugin.imagemin.svgo({plugins: [{removeViewBox: true}]})
        ]))
		.pipe(gulp.dest(path.build.img))
		.on('end', function(){ plugin.util.log(chalk.bgCyan('Изображения оптимизированы и скопированы в папку проекта')); })
		.pipe(plugin.browserSync.stream());
	// gulp.src(path.build.img + '**')
	// 	.pipe(ftpConnect.newerOrDifferentSize(path.build.img, { base: '.', buffer: false }))
	// 	.pipe(ftpConnect.dest(path.ftp + '/images'))
	// 	.on('end', function(){ plugin.util.log(chalk.blue('Изображения загружены на FTP')); });
});

gulp.task('fonts', function () {
	gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.build.fonts))
		.on('end', function(){
			plugin.util.log(chalk.magenta('Шрифты скопированы в папку проекта'));
		});
	// gulp.src(path.build.fonts + '**')
	// 	.pipe(ftpConnect.newerOrDifferentSize(path.build.fonts, { base: '.', buffer: false }))
	// 	.pipe(ftpConnect.dest(path.ftp + '/fonts'))
	// 	.on('end', function(){ plugin.util.log(chalk.blue('Шрифты загружены на FTP')); });
});

gulp.task('svg', function () {
	gulp.src(path.src.svg)
        // .pipe(plugin.newer(path.build.svg))
        // .pipe(plugin.svgmin())
        // .on('end', function(){ plugin.util.log(chalk.magenta('Минифицированы векторные изображения')); })
		.pipe(gulp.dest(path.build.svg))
		.on('end', function(){ 
			plugin.util.log(chalk.bgMagenta('Векторы SVG скопированы в папку проекта')); })
		.pipe(plugin.browserSync.stream());
});

gulp.task('watch', function () {
	plugin.watch([path.watch.html], function (event, cb) {
		gulp.start('html');
	});

	plugin.watch([path.watch.php], function (event, cb) {
		gulp.start('php');
	});

	plugin.watch([path.watch.js], function (event, cb) {
		gulp.start('js');
	});

	plugin.watch([path.watch.css], function (event, cb) {
		gulp.start('css');
	});

	plugin.watch([path.watch.img], function (event, cb) {
		gulp.start('img');
	});

  plugin.watch([path.watch.svg], function (event, cb) {
		gulp.start('svg');
	});

	plugin.watch([path.watch.fonts], function (event, cb) {
		gulp.start('fonts');
	});
});

gulp.task('watchjs', function () {
	plugin.watch([path.watch.js], function (event, cb) {
		gulp.start('js');
	});
});

gulp.task('clean', function (cb) {
	plugin.rimraf(path.clean, cb);
});

gulp.task('default', [
	'fonts',
	'php',
	'img',
	'html',
	'js',
  'svg',
	'css',
	'webserver',
	'watch'
]);
